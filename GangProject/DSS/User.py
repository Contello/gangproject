from threading import Lock

class User:
    def __init__(self,name):
        self.name = name
        self.songDict = {}
        self.dictLock = Lock()

    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return self.name != other.name

    def __len__(self):
        return len(self.songDict)

    def printUser(self):
        #self.dictLock.acquire()
        print self.name
        for song in self.songDict:
            print song + " Score: " + str(self.songDict[song])
        #self.dictLock.release()

    def addSong(self, id, score):   #fa anche l'update
        #self.dictLock.acquire()
        self.songDict[id] = score
        #self.dictLock.release()

    def removeSong(self, id):
        #self.dictLock.acquire()
        if id in self.songDict:
            del self.songDict[id]
        #self.dictLock.release()