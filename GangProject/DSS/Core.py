from User import *
from Wrapper import *
from pprint import pprint
from threading import Lock
import thread
import time
from cplexCore import CplexCore

class Core:
    def __init__(self):
        self.userlist = []
        self.userListLock = Lock()
        self.wrapper = Wrapper(1000,self)
        self.wrapper.daemon = True
        self.wrapper.start()
        self.playlist = []
        self.blacklist = []

    def addSong(self,id,user,score):
        self.userListLock.acquire()
        #print 'addSong'
        if user not in self.userlist:
            user.addSong(id,score)
            self.userlist.append(user)
        else:
            user = self.userlist[self.userlist.index(user)]
            user.addSong(id,score)
        self.userListLock.release()

    def removeSong(self,id,user):
        self.userListLock.acquire()
        #print 'removeSong'
        if user not in self.userlist:
            #print 'user',user,'not present'
            self.userListLock.release()
            return
        else:
            #print 'removing song',id,'from user',user
            user = self.userlist[self.userlist.index(user)]
            user.removeSong(id)
            if len(user) == 0:
                self.userlist.remove(user)
        self.userListLock.release()

    def printUsers(self):
        self.userListLock.acquire()
        for user in self.userlist:
            user.printUser()
        self.userListLock.release()

    def calcPlaylist(self):
        self.userListLock.acquire()
        print 'calcPlaylist'
        self.playlist = []
        for user in self.userlist:
            for song in user.songDict:
                s=self.searchSong(song)
                if song not in self.blacklist:
                    if s is not None:
                        s[1]+=user.songDict[song]
                    else:
                        self.playlist.append([song,user.songDict[song]])
        self.playlist = sorted(self.playlist, key=lambda t: t[1], reverse=True)
        self.userListLock.release()

    def calcWithBlacklist(self):
        pprint(self.blacklist)
        self.userListLock.acquire()
        print 'calcWithBlacklist'
        self.playlist = []
        for user in self.userlist:
            for song in user.songDict:
                s = self.searchSong(song)
                if song not in self.blacklist:
                    if s is not None:
                        s[1] += user.songDict[song]
                    else:
                        self.playlist.append([song, user.songDict[song]])
        self.playlist = sorted(self.playlist, key=lambda t: t[1], reverse=True)
        self.blacklist = []
        i=0
        for song in self.playlist:
            if i<3:
                self.blacklist.append(song[0])
                pprint(song)
                i+=1
            else:
                break
        self.userListLock.release()

    def sendToWrapper(self,doc_type):
        self.userListLock.acquire()
        print 'sendToWrapper', doc_type
        try:
            self.wrapper.savePlaylist(list(self.playlist), doc_type)
            self.userListLock.release()
        except Exception as inst:
            self.userListLock.release()
            print inst, type(inst)

    def searchSong(self,song):
        for s in self.playlist:
            if song == s[0]:
                return s
        return None

    def searchBlacklistSong(self,song):
        for s in self.blacklist:
            if song == s[0]:
                return s
        return None
    
    def getScoreArray(self):
        s=[]
        for song in self.playlist:
            s.append(song[1])
        return s
    
    def removeUnselectedSongs(self,songSelected):
        i = 0
        for song in self.playlist:
            if songSelected[i]==0:
                self.playlist.remove(song)
            i+=1
      
    def printPlaylist(self):
        for song in self.playlist:
            print song      
            
def savePlaylistThread(core):
    try:
        while True:
            core.calcWithBlacklist()
            core.sendToWrapper('track')
            print "thread saveplaylist"
            time.sleep(12)
    except Exception as inst:
        print inst, type(inst)

if __name__ == '__main__':
    core = Core()
    saveThread = threading.Thread(target=savePlaylistThread, args=(core,))
    saveThread.daemon = True
    saveThread.start()
    while True:
        core.calcPlaylist()
        #core.printPlaylist()
        s=core.getScoreArray()
        
        cplex = CplexCore([75, 61, 58, 54, 53, 52, 51, 50, 50, 49, 49, 47, 47, 47, 44, 43, 42, 40, 40, 39, 38, 37, 37, 35, 34, 33, 30, 28, 27, 24, 24, 23, 22, 21, 21])
        #s=cplex.getScores()
        #print s
        s=cplex.calculate(3)
        print s
        #songSelected=cplex.calculate(3)
        #core.removeUnselectedSongs(songSelected)
        #core.printPlaylist()
        core.sendToWrapper('track_playlist')
        time.sleep(5)