import threading
from couchdbkit import Server, ChangesStream
from User import *
import json, httplib
import SongToJSON
from pprint import pprint

class Wrapper(threading.Thread):
    def __init__(self, heartbeat, core):
        threading.Thread.__init__(self)
        self.s = Server()  # apre la connessione col server in locale in questo cas
        # se vogliamo possiamo specificare l'indirizzo specifico

        self.db = self.s["gangproject"]
        self.heartbeat = heartbeat
        self.core = core
        self.keepGoing = True

    def run(self):
        stream = ChangesStream(self.db, feed="continuous", heartbeat=self.heartbeat, include_docs=True)

        for change in stream:
            if self.core is not None:
                doc = change['doc']
                #print 'change: ',doc
                if 'deleted' not in change:
                    if 'type' in doc:
                        if doc['type'] == "user_songs_playlist":
                            #print 'change: new doc'
                            id = doc['_id']
                            user = id.split("_", 1)[0]
                            song = id.split("_", 1)[1]
                            score = 11 - doc['position']
                            self.core.addSong(song,User(user),score)
                else:
                    #print 'change: delete doc'
                    id = doc['_id']
                    if '_' in id:   #per gestire la cancellazione degli user, da rivedere
                        user = id.split("_",1)[0]
                        song = id.split("_",1)[1]
                        self.core.removeSong(song,User(user))

    def querySong(self, id):
        js = json.dumps({"selector": {"link": "https://open.spotify.com/track/" + id}})
        headers = {"Content-type": "application/json"}
        c = httplib.HTTPConnection('127.0.0.1', 5984)   #l'ip dovrebbe essere un parametro
        c.connect()
        c.request("POST", "/gangproject/_find", js, headers)
        res = c.getresponse()
        data = res.read()
        #print data, type(data)
        c.close()
        res_json = json.loads(data)
        if len(res_json['docs']) > 0:
            return res_json['docs'][0]
        return None

    def savePlaylist(self,playlist,doc_type):
        print 'savePlaylist', doc_type
        id_str = 'pos'
        i = 0
        for couple in playlist:
            song_id = couple[0]
            score = couple[1]
            song_data = self.querySong(song_id)
            del song_data['_rev']
            if 'position' in song_data:
                del song_data['position']
            if 'position_old' in song_data:
                del song_data['position_old']
            song_data['score'] = score
            if doc_type == 'track_playlist':
                song_data['type'] = 'track_playlist'
                song_data['_id'] = 'pos' + str(i+1)
            elif doc_type == 'track':
                song_data['type'] = 'track_' + str(i+1)
                song_data['_id'] = 'track' + str(i+1)
            SongToJSON.save2(song_data)
            i += 1
            if i == 3:
                break
