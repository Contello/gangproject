#inserimento di una canzone dall'id pos1 nel db
from couchdbkit import *
from pprint import pprint
import httplib, json

class SongToJSON(Document):
        link = StringProperty()
        artist = StringProperty()
        name = StringProperty()
        album = StringProperty()
        cover = StringProperty()
        score = IntegerProperty()
        _doc_type_attr = 'type'     #il nome dell'attributo che conserva il tipo
        
SongToJSON._doc_type = 'track_playlist'    #il valore da assegnare a type


def save(link, artist, name, album, cover, score, id):
        s = Server()
        db = s['gangproject']

        SongToJSON.set_db(db)


        song1 = SongToJSON.get_or_create(
                docid=id)  # legge il documento con docid=id  dal db, se non esiste lo crea
        song1['link'] = link
        song1['artist'] = artist
        song1['name'] = name
        song1['album'] = album
        song1['cover'] = cover
        song1['score'] = score
        song1.save()
'''
def save2(song_struct):
    s = Server()
    db = s['gangproject']
    doc_id = song_struct['_id']
    if doc_id in db:
        del db[doc_id]
        print 'deleted',doc_id
    doc = Document.from_json(song_struct)
    doc.set_db(db)
    doc.save()
    print 'saved',doc_id
'''

def save2(song_struct):
    conn = httplib.HTTPConnection('127.0.0.1',5984)
    conn.request('GET','/gangproject/' + song_struct['_id'])
    res = conn.getresponse()
    data = res.read()
    rev = None
    if res.status == 200:
        js = json.loads(data)
        rev = js['_rev']
    elif res.status != 404:
        print 'error: ', res.status, res.reason
        conn.close()
        return
    if rev is not None:
        song_struct['_rev'] = rev
    conn.request('POST','/gangproject',json.dumps(song_struct),headers={'Content-Type': 'application/json'})
    res = conn.getresponse()
    #print res.status, res.reason
    conn.close()