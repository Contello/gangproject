class Song():
    def __init__(self, link, artist, name, album, cover, score = 0):
        self.link = link
        self.artist = artist
        self.name = name
        self.album = album
        self.cover = cover
        self.score = score
        self.type = "track_playlist"
        self.modifier = 0

    def __eq__(self, other):
        #implementa il confronto fra oggetti song
        #guarda il link di spotify
        return self.link == other.link

    def __str__(self):
        #per stampare l'oggetto
        return self.artist + "\n" + self.name + "\nScore: " + str(self.score) + "\n" + self.link