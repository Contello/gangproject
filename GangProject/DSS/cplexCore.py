'''
Created on 25 ago 2017
'''

from __future__ import print_function

import cplex
from cplex.exceptions import CplexError
from pprint import pprint

class CplexCore:

    def __init__(self, scores):
        self.scores = scores
    
    def setScores(self, scores):
        self.scores = scores
        
    def getScores(self):
        return self.scores
        
    def calculate(self, t):
        try:
            prob = cplex.Cplex()
            songsNumber = len(self.scores)
            pprint(self.scores)

            prob.objective.set_sense(prob.objective.sense.maximize)
            # obj: coefficienti delle canzoni(punteggi)
            # ub: valore massimo delle variabili(1 in questo caso)
            # types: le variabili sono interi
            prob.variables.add(obj=self.scores, ub=[1]*songsNumber, types=[prob.variables.type.integer] * songsNumber)
            
            # definisce un solo vincolo, la prima parte indica gli indici delle variabili da considerare
            # la seconda indica i coefficienti da applicare alle variabili(1 in questo caso)
            # senses indica il tipo di vincolo: E indica uguaglianza
            # rhs indica la parte destra delle equazioni dei vincoli
            rows = [[[x for x in range(songsNumber)],
                     [1.0] * songsNumber]]
            prob.linear_constraints.add(lin_expr=rows, senses="E", rhs=[t])

            prob.solve()
        except CplexError as exc:
            print(exc)
            return
        
        print("Solution status = ", prob.solution.get_status(), ":", end=' ')
        print(prob.solution.status[prob.solution.get_status()])
        print("Solution value  = ", prob.solution.get_objective_value())
        
        x = prob.solution.get_values()
        print("Chosen songs = ", x)
        prob.write("prob.lp")
        return x
        
    
    def calculateByTime(self, t,duration):
        try:
            prob = cplex.Cplex()
            songsNumber = len(self.scores)
            prob.objective.set_sense(prob.objective.sense.maximize)
            # obj: coefficienti delle canzoni(punteggi)
            # ub: valore massimo delle variabili(1 in questo caso)
            # types: le variabili sono interi
            prob.variables.add(obj=self.scores, ub=[1]*songsNumber, types=[prob.variables.type.integer] * songsNumber)
            
            # definisce un solo vincolo, la prima parte indica gli indici delle variabili da considerare
            # la seconda indica i coefficienti da applicare alle variabili(1 in questo caso)
            # senses indica il tipo di vincolo: E indica uguaglianza
            # rhs indica la parte destra delle equazioni dei vincoli
            rows = [[[x for x in range(songsNumber)],
                     duration]]
            prob.linear_constraints.add(lin_expr=rows, senses="L", rhs=[t])
            prob.solve()
        except CplexError as exc:
            print(exc)
            return
        
        print("Solution status = ", prob.solution.get_status(), ":", end=' ')
        print(prob.solution.status[prob.solution.get_status()])
        print("Solution value  = ", prob.solution.get_objective_value())
        
        x = prob.solution.get_values()
        print("Chosen songs = ", x)
        prob.write("prob.lp")
        
